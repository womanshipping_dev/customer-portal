<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dashboard</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
    <div class="container">
        <div class="row d-flex justify-content-between align-items-center">
            <div class="col-md-4">
                <h2>Buenos dias, usuario</h2>
            </div>
            <div class="col-md-2">
                <form method="POST" action="./create_customer_portal_session.php">
                    <button type="submit" class="btn btn-primary">Manage billing</button>
                </form>
            </div>
        </div>
    </div>
</body>
</html>