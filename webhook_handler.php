<?php
require_once('./vendor/autoload.php');

\Stripe\Stripe::setApiKey('sk_test_51H5SoCGpVWsFX2P9iozmLejl5icpwkaxmUsDR4IEzlZ3hjLn1L7K4eP1I1t0zDAI8Z7RlFLNrfDXTo8kcZa7McLX001HcoVpeC');

$endpoint_secret = 'whsec_vQjAGoNQgLRyxhr0m89qi4As2Km0vx8O';

$payload = @file_get_contents('php://input');
$sig_header = $_SERVER['HTTP_STRIPE_SIGNATURE'];
$event = null;

try {
    $event = \Stripe\Webhook::constructEvent(
        $payload, $sig_header, $endpoint_secret
    );
} catch(\UnexpectedValueException $e) {
    // Invalid payload
    http_response_code(400);
    exit();
} catch(\Stripe\Exception\SignatureVerificationException $e) {
    // Invalid signature
    http_response_code(400);
    exit();
}

// Handle the event
switch ($event->type) {
    case 'customer.subscription.deleted':
        $subscriptionDeleted = $event->data->object;
        handleSubscriptionDeleted($subscriptionDeleted);
        break;
    default:
        // Unexpected event type
        http_response_code(400);
        exit();
}

function handleSubscriptionDeleted($event) {
    file_put_contents(
        'registro.log',
         $event->customer. " Ha cancelado la suscripcion\n",
        FILE_APPEND
      );
}
http_response_code(200);